# Duke Of Code Miniblog

1. Set the startup project to be the Web.
1. From the package manager console run ```Update-Database``` 
![Migrations](https://bitbucket.org/mariosshiatis/doc-miniblog/raw/322355dfab9b1ac062217f6ba809a93e4fc78ece/Documentation/Apply%20Migrations.JPG)
1. Set multple startup projects, to run the API and the Web Project at the same time.
![Run](https://bitbucket.org/mariosshiatis/doc-miniblog/raw/322355dfab9b1ac062217f6ba809a93e4fc78ece/Documentation/Run%20Project.JPG)
1. Check the cors policy in API, and add your local host if needed. 