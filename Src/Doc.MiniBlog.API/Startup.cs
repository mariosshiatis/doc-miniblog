﻿using AutoMapper;
using DoC.MiniBlog.Data;
using DoC.MiniBlog.Data.Repositories.Article;
using DoC.MiniBlog.Data.Repositories.ArticlesLikes;
using DoC.MiniBlog.Data.Repositories.Comment;
using DoC.MiniBlog.Services.Article;
using DoC.MiniBlog.Services.ArticlesLikes;
using DoC.MiniBlog.Services.Comment;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;

namespace DoC.MiniBlog.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //Add connection
            services.AddDbContext<MiniBlogDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")), ServiceLifetime.Transient);
            //Add dependencies 
            services.AddScoped<IArticlesService, ArticlesService>();
            services.AddScoped<IArticlesRepository, ArticlesRepository>();
            services.AddScoped<IArticleLikesRepository, ArticleLikesRepository>();
            services.AddScoped<IArticleLikesService, ArticleLikesService>();
            services.AddScoped<ICommentsService, CommentsService>();
            services.AddScoped<ICommentsRepository, CommentsRepository>();
            services.AddAutoMapper(cfg => cfg.ValidateInlineMaps = false);
            services.AddSwaggerGen(c => c.SwaggerDoc("v1", new Info { Title = "DoCMiniBlog", Version = "v1" }));
            services.AddCors(options =>
            options.AddPolicy("AllowAll", p =>
            {
                p.AllowAnyOrigin();
                p.AllowAnyMethod();
                p.AllowAnyHeader();
            }));
            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseCors(opt => { opt.WithOrigins("http://localhost:56132").AllowAnyHeader().AllowAnyMethod(); });
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => { c.SwaggerEndpoint("/swagger/v1/swagger.json", "DoCMiniBlog"); });
            }

            app.UseMvc();
        }
    }
}
