﻿using DoC.MiniBlog.Common.Dtos;
using DoC.MiniBlog.Services.Comment;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DoC.MiniBlog.Api.Controllers
{
    [Route("api/[controller]/[action]/{id?}")]
    public class CommentsController : Controller
    {
        private readonly ICommentsService _commentsService;

        public CommentsController(ICommentsService commentsService)
        {
            _commentsService = commentsService;
        }

        [HttpGet]
        public async Task<List<CommentsDto>> GetCommentsByArticleId(Guid id)
        {
            return await _commentsService.GetCommentsByArticleId(id);
        }

        [HttpPost]
        public async Task AddArticleComment([FromBody]CommentsDto comment)
        {
            await _commentsService.AddArticleComment(comment);
        }
    }
}