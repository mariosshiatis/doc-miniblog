﻿using DoC.MiniBlog.Common.Dtos;
using DoC.MiniBlog.Services.Article;
using DoC.MiniBlog.Services.ArticlesLikes;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DoC.MiniBlog.Api.Controllers
{
    [Route("api/[controller]/[action]/{id?}")]
    public class ArticlesController : Controller
    {
        private readonly IArticlesService _articlesService;
        private readonly IArticleLikesService _articleLikesService;

        public ArticlesController(IArticlesService articlesService,
            IArticleLikesService articleLikesService)
        {
            _articlesService = articlesService;
            _articleLikesService = articleLikesService;
        }

        [HttpGet]
        public async Task<IEnumerable<ArticleDto>> GetAllArticles()
        {
            return await _articlesService.GetAllOrderByDate();
        }

        [HttpGet]
        public async Task<int> GetArticleLikes(Guid id)
        {
            return await _articleLikesService.GetArticleLikes(id);
        }

        [HttpPost]
        public async Task LikeArticle([FromBody]ArticleLikesDto articleLikesDto)
        {
            await _articleLikesService.LikeArticle(articleLikesDto);
        }

    }
}
