﻿using AutoMapper;
using DMS.Models;
using DoC.MiniBlog.Common.Dtos;

namespace DMS.Mappings
{
    public class Mappings : Profile
    {
        public Mappings(){
            CreateMap<ArticleViewModel, ArticleDto>().ReverseMap();
        }
    }
}