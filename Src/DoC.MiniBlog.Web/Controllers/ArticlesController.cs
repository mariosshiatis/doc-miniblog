﻿using AutoMapper;
using DMS.Models;
using DoC.MiniBlog.Common.Dtos;
using DoC.MiniBlog.Services.Article;
using DoC.MiniBlog.Services.Category;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DoC.MiniBlog.Web.Controllers
{
    [Authorize(Roles = "Publisher")]
    public class ArticlesController : Controller
    {
        private readonly IArticlesService _articlesService;
        private readonly ICategoriesService _categoriesService;

        public ArticlesController(IArticlesService articleService,
            ICategoriesService categoriesService)
        {
            _articlesService = articleService;
            _categoriesService = categoriesService;
        }

        // GET: ArticleViewModel
        public async Task<IActionResult> Index()
        {
            var allArticleViewModel = await _articlesService.GetAllOrderByDate();
            ViewData["Categories"] = await _categoriesService.GetCategoriesAsync();
            return View(Mapper.Map<List<ArticleViewModel>>(allArticleViewModel));
        }

        // GET: ArticleViewModel/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var article = await _articlesService.GetDetailsById(id);

            if (article == null)
            {
                return NotFound();
            }
            ViewData["Categories"] = await _categoriesService.GetCategoriesAsync();
            return View(Mapper.Map<ArticleViewModel>(article));
        }

        //// GET: ArticleViewModel/Create
        public async Task<IActionResult> Create()
        {
            ViewData["CategoryId"] = new SelectList(await _categoriesService.GetCategoriesAsync(), "Id", "Title");
            ArticleViewModel article = new ArticleViewModel();
            article.PublishDate = DateTime.Now;
            return View(article);
        }

        // POST: ArticleViewModel/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Title,Body,PublishDate,Author,Images,CategoryId,Tags")] ArticleViewModel ArticleViewModel)
        {
            if (ModelState.IsValid)
            {

                await _articlesService.CreateAsync(Mapper.Map<ArticleDto>(ArticleViewModel));

                return RedirectToAction(nameof(Index));
            }
            ViewData["CategoryId"] = new SelectList(await _categoriesService.GetCategoriesAsync(), "Id", "Title", ArticleViewModel.CategoryId);
            return View(ArticleViewModel);
        }

        //// GET: ArticleViewModel/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var article = await _articlesService.GetDetailsById(id);
            if (article == null)
            {
                return NotFound();
            }
            ViewData["CategoryId"] = new SelectList(await _categoriesService.GetCategoriesAsync(), "Id", "Title", article.CategoryId);
            return View(Mapper.Map<ArticleViewModel>(article));
        }

        // POST: ArticleViewModel/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, [Bind("Id,Title,Body,PublishDate,Author,Images,CategoryId,Tags")] ArticleViewModel article)
        {
            if (id != article.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    await _articlesService.EditAsync(id, Mapper.Map<ArticleDto>(article));

                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!await ArticleViewModelExists(article.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["CategoryId"] = new SelectList(await _categoriesService.GetCategoriesAsync(), "Id", "Title", article.CategoryId);
            return View(article);
        }

        // GET: ArticleViewModel/Delete/5
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var article = await _articlesService.GetDetailsById(id);
            ViewData["CategoryId"] = new SelectList(await _categoriesService.GetCategoriesAsync(), "Id", "Title", article.CategoryId);

            return View(Mapper.Map<ArticleViewModel>(article));
        }

        // POST: ArticleViewModel/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            await _articlesService.DeleteAsync(id);

            return RedirectToAction(nameof(Index));
        }

        private async Task<bool> ArticleViewModelExists(Guid id)
        {
            return await _articlesService.GetDetailsById(id) != null;
        }
    }
}
