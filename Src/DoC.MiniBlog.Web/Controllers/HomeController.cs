using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;

namespace DoC.MiniBlog.Web.Controllers
{
    public class HomeController : Controller
    {
        [Authorize(Roles = "Publisher,Employee")]
        public IActionResult Index()
        {

            return View();
        }

        public IActionResult Error()
        {
            ViewData["RequestId"] = Activity.Current?.Id ?? HttpContext.TraceIdentifier;
            return View();
        }
    }
}
