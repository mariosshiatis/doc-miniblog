import { Component } from '@angular/core';
import { ConfigService } from '../../config.service';

@Component({
    selector: 'nav-menu',
    templateUrl: './navmenu.component.html',
    styleUrls: ['./navmenu.component.css']
})
export class NavMenuComponent {

    constructor(public config: ConfigService) {
    }
}
