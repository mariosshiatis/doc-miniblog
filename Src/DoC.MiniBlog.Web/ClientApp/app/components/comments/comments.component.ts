﻿import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { ConfigService } from '../../config.service';
import { ActivatedRoute } from '@angular/router';
import { Comments } from '../../models/comments';

@Component({
    selector: 'app-comments',
    templateUrl: './comments.component.html',
    styleUrls: ['./comments.component.css']
})
/** comments component*/
export class CommentsComponent implements OnInit {
    public comments: Comments[];
    public comment: Comments = new Comments();
    private id: string;
   
    /** comments ctor */
    constructor(private http: Http, private config: ConfigService, private route: ActivatedRoute) {        
    }

    ngOnInit(): void {
        this.route.params.subscribe(params => {
            this.id = this.config.articles[params['id']].id;
            this.http.get(this.config.api_url + '/api/Comments/GetCommentsByArticleId/' + this.id).subscribe(result => {
                this.comments = result.json() as Comments[];

            }, error => console.error(error));

        });
    }

    addComment() {
        if (this.comment == undefined || this.comment == null || this.comment.comment==null)
            return;
        this.comment.articleId = this.id; 
        this.comment.username = this.config.username;
        this.comment.time = new Date();
        this.http.post(this.config.api_url + '/api/Comments/AddArticleComment/', this.comment).subscribe(result => {
            //refresh likes after clicking
            this.ngOnInit();

        }, error => console.error(error));
    }
}