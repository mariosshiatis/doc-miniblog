import { Component, OnInit, ViewChild, ElementRef, Input } from '@angular/core';
import { ConfigService } from '../../config.service';

@Component({
    selector: 'app',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})


export class AppComponent implements OnInit{

    constructor(private elementRef: ElementRef, private config: ConfigService) {
    }

    ngOnInit() {
        debugger;
        let el = document.getElementById('role');

        if (el !== null) {
            this.config.isPublisher = (el.dataset.role == 'True') ? true : false;
            this.config.username = (el.dataset.username == undefined) ? '' : el.dataset.username;
        }
    }

    getRole(role:string) {
        
        alert(role);
    }
}
