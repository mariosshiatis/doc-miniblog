﻿import { Component, Inject, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { ConfigService } from '../../config.service';
import { Article } from '../../models/article';

@Component({
    selector: 'app-articles',
    templateUrl: './articles.component.html',
    styleUrls: ['./articles.component.css']
})
/** articles component*/
export class ArticlesComponent  implements OnInit{
    /** articles ctor */
    public articles: Article[];

    constructor(private http: Http,private config: ConfigService) {
        
    }
    ngOnInit(): void {
        this.http.get(this.config.api_url + '/api/Articles/GetAllArticles').subscribe(result => {
            this.articles = result.json() as Article[];
            this.config.articles = this.articles; 
        }, error => console.error(error));
    }
}