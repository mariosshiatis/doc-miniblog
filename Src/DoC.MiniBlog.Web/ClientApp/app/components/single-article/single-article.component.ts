﻿import { Component } from '@angular/core';
import { ConfigService } from '../../config.service';
import { Article } from '../../models/article';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
    selector: 'app-single-article',
    templateUrl: './single-article.component.html',
    styleUrls: ['./single-article.component.css']
})
/** single-article component*/
export class SingleArticleComponent {

    public article: Article;
    /** single-article ctor */
    constructor(private config: ConfigService, private route: ActivatedRoute, private router:Router) {
        this.route.params.subscribe(params => {
            //if no articles found redirect to main view
            if (config.articles == undefined)
                this.router.navigate(['/']);

            this.article = config.articles[params['id']];
        });
    }
}