﻿/// <reference path="../../../../node_modules/@types/jasmine/index.d.ts" />
import { TestBed, async, ComponentFixture, ComponentFixtureAutoDetect } from '@angular/core/testing';
import { BrowserModule, By } from "@angular/platform-browser";
import { SingleArticleComponent } from './single-article.component';

let component: SingleArticleComponent;
let fixture: ComponentFixture<SingleArticleComponent>;

describe('single-article component', () => {
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ SingleArticleComponent ],
            imports: [ BrowserModule ],
            providers: [
                { provide: ComponentFixtureAutoDetect, useValue: true }
            ]
        });
        fixture = TestBed.createComponent(SingleArticleComponent);
        component = fixture.componentInstance;
    }));

    it('should do something', async(() => {
        expect(true).toEqual(true);
    }));
});