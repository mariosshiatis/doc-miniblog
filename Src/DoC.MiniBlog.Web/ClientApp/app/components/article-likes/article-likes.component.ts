﻿import { Component, OnInit } from '@angular/core';
import { ConfigService } from '../../config.service';
import { Http } from '@angular/http';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-article-likes',
    templateUrl: './article-likes.component.html',
    styleUrls: ['./article-likes.component.css']
})
/** article-likes component*/
export class ArticleLikesComponent implements OnInit {
    public id: string;
    public articleLikes: number;
    /** article-likes ctor */
    constructor(private http: Http, private config: ConfigService, private route: ActivatedRoute) {

    }

    ngOnInit(): void {
        this.route.params.subscribe(params => {
             this.id = this.config.articles[params['id']].id;
            this.http.get(this.config.api_url + '/api/Articles/GetArticleLikes/' + this.id).subscribe(result => {
                this.articleLikes = result.json() as number;

            }, error => console.error(error));

        });
    }

    likeArticle() {
        this.http.post(this.config.api_url + '/api/Articles/LikeArticle/', { username: this.config.username, articleId: this.id }).subscribe(result => {
            //refresh likes after clicking
            this.ngOnInit();
           
        }, error => console.error(error));
    }
}