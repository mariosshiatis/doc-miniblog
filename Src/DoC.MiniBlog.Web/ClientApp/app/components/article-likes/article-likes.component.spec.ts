﻿/// <reference path="../../../../node_modules/@types/jasmine/index.d.ts" />
import { TestBed, async, ComponentFixture, ComponentFixtureAutoDetect } from '@angular/core/testing';
import { BrowserModule, By } from "@angular/platform-browser";
import { ArticleLikesComponent } from './article-likes.component';

let component: ArticleLikesComponent;
let fixture: ComponentFixture<ArticleLikesComponent>;

describe('article-likes component', () => {
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ ArticleLikesComponent ],
            imports: [ BrowserModule ],
            providers: [
                { provide: ComponentFixtureAutoDetect, useValue: true }
            ]
        });
        fixture = TestBed.createComponent(ArticleLikesComponent);
        component = fixture.componentInstance;
    }));

    it('should do something', async(() => {
        expect(true).toEqual(true);
    }));
});