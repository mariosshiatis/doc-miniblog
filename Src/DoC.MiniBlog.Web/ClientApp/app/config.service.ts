﻿import { Injectable } from "@angular/core";
import { Article } from "./models/article";

@Injectable()
export class ConfigService
{
    isPublisher: boolean = false;
    username: string | undefined;
    articles: Article[];
    public api_url = "http://localhost:53774";
}