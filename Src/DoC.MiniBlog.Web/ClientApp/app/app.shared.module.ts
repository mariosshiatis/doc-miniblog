import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './components/app/app.component';
import { NavMenuComponent } from './components/navmenu/navmenu.component';
import { HomeComponent } from './components/home/home.component';
import { FetchDataComponent } from './components/fetchdata/fetchdata.component';
import { CounterComponent } from './components/counter/counter.component';
import { ArticlesComponent } from './components/articles/articles.component';
import { ConfigService } from './config.service';
import { SingleArticleComponent } from './components/single-article/single-article.component';
import { ArticleLikesComponent } from './components/article-likes/article-likes.component';
import { CommentsComponent } from './components/comments/comments.component';

@NgModule({
    declarations: [
        AppComponent,
        NavMenuComponent,
        CounterComponent,
        FetchDataComponent,
        ArticlesComponent,
        HomeComponent,
        SingleArticleComponent,
        ArticleLikesComponent,
        CommentsComponent
    ],
    providers: [
        ConfigService,],
    imports: [
        CommonModule,
        HttpModule,
        FormsModule,
        RouterModule.forRoot([
            { path: '', redirectTo: 'home', pathMatch: 'full' },
            { path: 'home', component: ArticlesComponent },
            { path: 'article/:id', component: SingleArticleComponent },
            { path: '**', redirectTo: 'home' }
        ])
    ]
})
export class AppModuleShared {
}
