﻿export class Article {
    id: string;
    title: string;
    suthor: string;
    body: string;
    images: string;
    publishDate: Date;
    tags: string;
    categoryId: string;
}