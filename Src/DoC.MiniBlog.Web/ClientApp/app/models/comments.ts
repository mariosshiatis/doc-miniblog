﻿export class Comments {
    id: string;
    comment: string;
    username: string | undefined;
    articleId: string;
    time: Date;
}