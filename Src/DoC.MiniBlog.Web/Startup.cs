using AutoMapper;
using DoC.MiniBlog.Data;
using DoC.MiniBlog.Data.Repositories.Article;
using DoC.MiniBlog.Data.Repositories.Category;
using DoC.MiniBlog.DataAccess.Seed;
using DoC.MiniBlog.Services.Article;
using DoC.MiniBlog.Services.Category;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.SpaServices.Webpack;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace DoC.MiniBlog.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public async void ConfigureServices(IServiceCollection services)
        {
            //Add connection
            services.AddDbContext<MiniBlogDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")), ServiceLifetime.Transient);

            //Add Identity framework
            services.AddIdentity<MiniBlogUser, IdentityRole>(options =>
            {
                options.SignIn.RequireConfirmedEmail = false;

            })
            .AddEntityFrameworkStores<MiniBlogDbContext>()
            .AddDefaultTokenProviders();

            //Add dependencies 
            services.AddScoped<IArticlesService, ArticlesService>();
            services.AddScoped<IArticlesRepository, ArticlesRepository>();
            services.AddScoped<ICategoriesRepository, CategoriesRepository>();
            services.AddScoped<ICategoriesService, CategoriesService>();
            services.AddAutoMapper();
            services.AddMvc();
            services.AddAuthentication();
            services.AddSession();
            services.AddTransient<DbInitializer>();

            var dbInit = services.BuildServiceProvider().GetRequiredService<DbInitializer>();
            await dbInit.Initialize();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseWebpackDevMiddleware(new WebpackDevMiddlewareOptions
                {
                    HotModuleReplacement = true
                });
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();
            app.UseAuthentication();
            app.UseSession();
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");

                routes.MapSpaFallbackRoute(
                    name: "spa-fallback",
                    defaults: new { controller = "Home", action = "Index" });
            });
        }
    }
}
