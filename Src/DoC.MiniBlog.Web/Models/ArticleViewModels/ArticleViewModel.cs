﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DMS.Models
{
    public class ArticleViewModel
    {
        public Guid Id { get; set; }

        [Required]
        [StringLength(150)]
        public string Title { get; set; }
        [Required]
        public string Body { get; set; }
        [Required]
        public DateTime PublishDate { get; set; }
        [Required]
        [StringLength(50)]
        public string Author { get; set; }
        [Required]
        public string Images { get; set; }
        [Required]
        public int CategoryId { get; set; }
        public List<SelectListItem> Categories { get; set; }
        [Required]
        [StringLength(100)]
        public string Tags { get; set; }
    }

}
