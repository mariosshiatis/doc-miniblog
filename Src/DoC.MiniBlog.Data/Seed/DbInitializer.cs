﻿using Bogus;
using DoC.MiniBlog.Data;
using DoC.MiniBlog.Common.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DoC.MiniBlog.DataAccess.Seed
{
    public class DbInitializer
    {
        private readonly MiniBlogDbContext _dbContext;
        private readonly UserManager<MiniBlogUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly string _publisherRole = "Publisher";
        private readonly string _employeeRole = "Employee";
        private readonly List<string> _categories = new List<string>(new string[] { "Category A", "Category B", "Category C" });



        public DbInitializer(MiniBlogDbContext dbContext,
            UserManager<MiniBlogUser> userManager,
            RoleManager<IdentityRole> roleManager)
        {
            _dbContext = dbContext;
            _userManager = userManager;
            _roleManager = roleManager;
        }


        public async Task Initialize()
        {
            await SeedRoles();
            await SeedUsers();
            await SeedCategories();
            await SeedArticles();
        }
        private async Task SeedArticles()
        {
            if (!_dbContext.Articles.Any())
            {

                var articlesFaker = new Faker<Articles>()
                    .RuleFor(u => u.Title, f => f.Lorem.Sentence(4))
                    .RuleFor(u => u.Body, f => f.Lorem.Paragraphs(8))
                    .RuleFor(u => u.Author, f => f.Person.FullName)
                    .RuleFor(u => u.Images, f => f.Image.Image())
                    .RuleFor(u => u.PublishDate, f => f.Date.Past())
                    .RuleFor(u => u.CategoryId, f => f.Random.Int(1, 3))
                    .RuleFor(u => u.Tags, f => string.Join(",", f.Lorem.Words(5).ToList()));

                var articles = articlesFaker.Generate(10);
                await _dbContext.Articles.AddRangeAsync(articles);
                await _dbContext.SaveChangesAsync();
            }
        }

        private async Task SeedCategories()
        {
            if (!_dbContext.Categories.Any())
            {
                var transaction = _dbContext.Database.BeginTransaction();
                var categories = new List<Categories>();
                _dbContext.Database.ExecuteSqlCommand("SET IDENTITY_INSERT [dbo].[Categories] ON");
                categories.Add(new Categories { Id = 1, Title = _categories.First() });
                categories.Add(new Categories { Id = 2, Title = _categories[1] });
                categories.Add(new Categories { Id = 3, Title = _categories[2] });
                await _dbContext.AddRangeAsync(categories);
                await _dbContext.SaveChangesAsync();
                _dbContext.Database.ExecuteSqlCommand("SET IDENTITY_INSERT [dbo].[Categories] OFF");
                transaction.Commit();
            }
        }

        private async Task SeedRoles()
        {
            if (!_dbContext.Roles.Any())
            {
                await _roleManager.CreateAsync(new IdentityRole() { Name = _employeeRole, NormalizedName = _employeeRole.ToUpper() });
                await _roleManager.CreateAsync(new IdentityRole() { Name = _publisherRole, NormalizedName = _publisherRole.ToUpper() });
            };
        }

        public async Task SeedUsers()
        {
            if (!_dbContext.Users.Any())
            {
                var admin = new MiniBlogUser { UserName = "admin@doc.com", Email = "admin@doc.com" };
                await _userManager.CreateAsync(admin, "TestAdmin123!");
                await _userManager.AddToRoleAsync(admin, _publisherRole);
                var employee = new MiniBlogUser { UserName = "employee@doc.com", Email = "employee@doc.com" };
                await _userManager.CreateAsync(employee, "TestEmployee123!");
                await _userManager.AddToRoleAsync(employee, _employeeRole);

            }
        }
    }
}
