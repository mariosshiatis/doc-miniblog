﻿using DoC.MiniBlog.Common.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DoC.MiniBlog.Data.Repositories.Category
{
    public interface ICategoriesRepository
    {
        Task<List<Categories>> GetCategoriesAsync();
    }
}
