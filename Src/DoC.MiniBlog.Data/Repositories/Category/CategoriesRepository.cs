﻿using DoC.MiniBlog.Common.Entities;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DoC.MiniBlog.Data.Repositories.Category
{
    public class CategoriesRepository : ICategoriesRepository
    {
        private readonly MiniBlogDbContext _dbContext;

        public CategoriesRepository(MiniBlogDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<List<Categories>> GetCategoriesAsync()
        {
            return await _dbContext.Categories.ToListAsync();
        }
    }
}