﻿using DoC.MiniBlog.Common.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DoC.MiniBlog.Data.Repositories.Comment
{
    public class CommentsRepository : ICommentsRepository
    {
        private readonly MiniBlogDbContext _dbContext;

        public CommentsRepository(MiniBlogDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        public async Task<List<Comments>> GetCommentsByArticleId(Guid id)
        {
            return await _dbContext.Comments.Where(comments => comments.ArticleId == id).ToListAsync();
        }

        public async Task AddArticleComment(Comments comment)
        {
            await _dbContext.Comments.AddAsync(comment);
            await _dbContext.SaveChangesAsync();
        }
    }
}
