﻿using DoC.MiniBlog.Common.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DoC.MiniBlog.Data.Repositories.Comment
{
    public interface ICommentsRepository
    {
        Task<List<Comments>> GetCommentsByArticleId(Guid id);

        Task AddArticleComment(Comments comment);
    }
}
