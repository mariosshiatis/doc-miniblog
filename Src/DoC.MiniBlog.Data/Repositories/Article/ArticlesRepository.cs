﻿using DoC.MiniBlog.Common.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DoC.MiniBlog.Data.Repositories.Article
{
    public class ArticlesRepository : IArticlesRepository
    {
        private readonly MiniBlogDbContext _dbContext;

        public ArticlesRepository(MiniBlogDbContext dbContext)
        {
            _dbContext = dbContext;

        }
        public async Task<List<Articles>> GetAllOrderByDate()
        {
            return await _dbContext.Articles.OrderByDescending(article => article.PublishDate).ToListAsync();
        }


        public async Task<Articles> GetDetailsById(Guid? id)
        {
            var article = await _dbContext.Articles
                .Include(a => a.Category)
                .SingleOrDefaultAsync(m => m.Id == id);

            return article;
        }

        public async Task<Articles> CreateAsync(Articles article)
        {
            _dbContext.Add(article);
            await _dbContext.SaveChangesAsync();
            return article;
        }

        public async Task EditAsync(Guid id, Articles article)
        {
            _dbContext.Update(article);
            await _dbContext.SaveChangesAsync();
        }

        public async Task DeleteAsync(Guid? id)
        {
            var articles = await _dbContext.Articles.SingleOrDefaultAsync(m => m.Id == id);
            _dbContext.Articles.Remove(articles);
            await _dbContext.SaveChangesAsync();
        }
    }
}
