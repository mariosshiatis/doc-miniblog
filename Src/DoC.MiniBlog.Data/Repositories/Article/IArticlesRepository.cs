﻿using DoC.MiniBlog.Common.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DoC.MiniBlog.Data.Repositories.Article
{
    public interface IArticlesRepository
    {
        Task<List<Articles>> GetAllOrderByDate();

        Task<Articles> GetDetailsById(Guid? id);

        Task<Articles> CreateAsync(Articles article);

        Task EditAsync(Guid id, Articles article);

        Task DeleteAsync(Guid? id);
    }
}
