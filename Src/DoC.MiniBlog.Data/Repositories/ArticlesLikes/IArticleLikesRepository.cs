﻿using DoC.MiniBlog.Common.Entities;
using System;
using System.Threading.Tasks;

namespace DoC.MiniBlog.Data.Repositories.ArticlesLikes
{
    public interface IArticleLikesRepository
    {
        Task<int> GetArticleLikes(Guid id);

        Task LikeArticle(ArticleLikes articleLikes);
    }
}