﻿using DoC.MiniBlog.Common.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace DoC.MiniBlog.Data.Repositories.ArticlesLikes
{
    public class ArticleLikesRepository : IArticleLikesRepository
    {
        private readonly MiniBlogDbContext _dbContext;

        public ArticleLikesRepository(MiniBlogDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        public async Task<int> GetArticleLikes(Guid id)
        {
            var articleLikes = await _dbContext.ArticleLikes.Where(likes => likes.ArticleId == id).CountAsync();
            return articleLikes;
        }

        public async Task LikeArticle(ArticleLikes articleLikes)
        {
            var alreadyLiked = await _dbContext.ArticleLikes
                      .AnyAsync(article => article.ArticleId == articleLikes.ArticleId && article.Username == articleLikes.Username);
            if (!alreadyLiked)
            {
                await _dbContext.ArticleLikes.AddAsync(articleLikes);
                await _dbContext.SaveChangesAsync();
            }
        }
    }
}
