﻿using DoC.MiniBlog.Common.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace DoC.MiniBlog.Data
{
    public class MiniBlogDbContext : IdentityDbContext<MiniBlogUser>
    {
        public MiniBlogDbContext(DbContextOptions<MiniBlogDbContext> options)
            : base(options)
        {

        }
        public DbSet<Categories> Categories { get; set; }
        public DbSet<Articles> Articles { get; set; }
        public DbSet<ArticleLikes> ArticleLikes { get; set; }
        public DbSet<Comments> Comments { get; set; }
    }
}