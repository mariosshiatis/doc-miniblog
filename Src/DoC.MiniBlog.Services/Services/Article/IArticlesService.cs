﻿using DoC.MiniBlog.Common.Dtos;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DoC.MiniBlog.Services.Article
{
    public interface IArticlesService
    {
        Task<List<ArticleDto>> GetAllOrderByDate();
        Task<ArticleDto> GetDetailsById(Guid? id);
        Task<ArticleDto> CreateAsync(ArticleDto article);
        Task EditAsync(Guid id, ArticleDto article);
        Task DeleteAsync(Guid? id);
    }
}
