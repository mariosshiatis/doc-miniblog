﻿using AutoMapper;
using DoC.MiniBlog.Common.Dtos;
using DoC.MiniBlog.Common.Entities;
using DoC.MiniBlog.Data.Repositories.Article;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DoC.MiniBlog.Services.Article
{
    public class ArticlesService : IArticlesService
    {
        private readonly IArticlesRepository _articlesRepository;

        public ArticlesService(IArticlesRepository articlesRepository)
        {
            _articlesRepository = articlesRepository;
        }

        public async Task<ArticleDto> CreateAsync(ArticleDto article)
        {
            var res = await _articlesRepository.CreateAsync(Mapper.Map<Articles>(article));
            return Mapper.Map<ArticleDto>(res);
        }

        public async Task DeleteAsync(Guid? id)
        {
            await _articlesRepository.DeleteAsync(id);

        }

        public async Task EditAsync(Guid id, ArticleDto article)
        {
            await _articlesRepository.EditAsync(id, Mapper.Map<Articles>(article));
        }

        public async Task<List<ArticleDto>> GetAllOrderByDate()
        {
            var res = await _articlesRepository.GetAllOrderByDate();
            return Mapper.Map<List<ArticleDto>>(res);
        }

        public async Task<ArticleDto> GetDetailsById(Guid? id)
        {
            var res = await _articlesRepository.GetDetailsById(id);
            return Mapper.Map<ArticleDto>(res);
        }
    }
}
