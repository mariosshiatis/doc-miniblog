﻿using AutoMapper;
using DoC.MiniBlog.Common.Dtos;
using DoC.MiniBlog.Common.Entities;
using DoC.MiniBlog.Data.Repositories.Comment;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DoC.MiniBlog.Services.Comment
{
    public class CommentsService : ICommentsService
    {
        private readonly ICommentsRepository _commentsRepository;

        public CommentsService(ICommentsRepository commentsRepository)
        {
            _commentsRepository = commentsRepository;
        }
        public async Task AddArticleComment(CommentsDto comment)
        {
            await _commentsRepository.AddArticleComment(Mapper.Map<Comments>(comment));
        }

        public async Task<List<CommentsDto>> GetCommentsByArticleId(Guid id)
        {
            var res = await _commentsRepository.GetCommentsByArticleId(id);
            return Mapper.Map<List<CommentsDto>>(res);
        }
    }
}
