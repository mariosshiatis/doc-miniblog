﻿using DoC.MiniBlog.Common.Dtos;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DoC.MiniBlog.Services.Comment
{
    public interface ICommentsService
    {
        Task<List<CommentsDto>> GetCommentsByArticleId(Guid id);

        Task AddArticleComment(CommentsDto comment);
    }
}
