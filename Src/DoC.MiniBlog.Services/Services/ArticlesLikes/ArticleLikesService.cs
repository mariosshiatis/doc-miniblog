﻿using AutoMapper;
using DoC.MiniBlog.Common.Dtos;
using DoC.MiniBlog.Common.Entities;
using DoC.MiniBlog.Data.Repositories.ArticlesLikes;
using System;
using System.Threading.Tasks;

namespace DoC.MiniBlog.Services.ArticlesLikes
{
    public class ArticleLikesService : IArticleLikesService
    {
        private readonly IArticleLikesRepository _articleLikesRepository;

        public ArticleLikesService(IArticleLikesRepository articleLikesRepository)
        {
            _articleLikesRepository = articleLikesRepository;
        }
        public async Task<int> GetArticleLikes(Guid id)
        {
            var res = await _articleLikesRepository.GetArticleLikes(id);
            return res;
        }

        public async Task LikeArticle(ArticleLikesDto articleLikesDto)
        {
            var articleLikes = Mapper.Map<ArticleLikes>(articleLikesDto);
            await _articleLikesRepository.LikeArticle(articleLikes);
        }
    }
}