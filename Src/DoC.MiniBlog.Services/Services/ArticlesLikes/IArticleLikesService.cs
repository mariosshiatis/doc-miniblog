﻿using DoC.MiniBlog.Common.Dtos;
using System;
using System.Threading.Tasks;

namespace DoC.MiniBlog.Services.ArticlesLikes
{
    public interface IArticleLikesService
    {
        Task<int> GetArticleLikes(Guid id);

        Task LikeArticle(ArticleLikesDto articleLikesDto);
    }
}
