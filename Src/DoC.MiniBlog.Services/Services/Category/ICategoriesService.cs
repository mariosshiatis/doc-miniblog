﻿using DoC.MiniBlog.Common.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DoC.MiniBlog.Services.Category
{
    public interface ICategoriesService
    {
        Task<List<CategoriesDto>> GetCategoriesAsync();
    }
}