﻿using AutoMapper;
using DoC.MiniBlog.Common.Dtos;
using DoC.MiniBlog.Data.Repositories.Category;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DoC.MiniBlog.Services.Category
{
    public class CategoriesService : ICategoriesService
    {
        private readonly ICategoriesRepository _categoriesRepository;

        public CategoriesService(ICategoriesRepository categoriesRepository)
        {
            _categoriesRepository = categoriesRepository;
        }
        public async Task<List<CategoriesDto>> GetCategoriesAsync()
        {
            var res = await _categoriesRepository.GetCategoriesAsync();
            return Mapper.Map<List<CategoriesDto>>(res);
        }
    }
}
