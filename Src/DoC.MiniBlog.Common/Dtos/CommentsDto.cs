﻿using System;

namespace DoC.MiniBlog.Common.Dtos
{
    public class CommentsDto
    {
        public Guid Id { get; set; }

        public string Comment { get; set; }

        public string Username { get; set; }

        public Guid ArticleId { get; set; }

        public DateTime Time { get; set; }
    }
}