﻿namespace DoC.MiniBlog.Common.Dtos
{
    public class CategoriesDto
    {
        public int Id { get; set; }

        public string Title { get; set; }
    }
}
