﻿using System;

namespace DoC.MiniBlog.Common.Dtos
{
    public class ArticleDto
    {
        public Guid Id { get; set; }
        public string Title { get; set; }

        public string Body { get; set; }

        public DateTime PublishDate { get; set; }

        public string Author { get; set; }

        public string Images { get; set; }

        public int CategoryId { get; set; }

        public string Tags { get; set; }
    }
}
