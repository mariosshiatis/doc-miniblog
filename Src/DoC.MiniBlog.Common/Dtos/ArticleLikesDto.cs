﻿using System;

namespace DoC.MiniBlog.Common.Dtos
{
    public class ArticleLikesDto
    {
        public string Username { get; set; }

        public Guid ArticleId { get; set; }
    }
}