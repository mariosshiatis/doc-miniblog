﻿using AutoMapper;
using DoC.MiniBlog.Common.Dtos;
using DoC.MiniBlog.Common.Entities;

namespace DoC.MiniBlog.Common.Mappings
{
    public class Mappings : Profile
    {
        public Mappings()
        {
            CreateMap<ArticleDto, Articles>().ReverseMap();
            CreateMap<Categories, CategoriesDto>().ReverseMap();
            CreateMap<ArticleLikes, ArticleLikesDto>().ReverseMap();
            CreateMap<CommentsDto, Comments>(MemberList.None)
                .ForMember(prop => prop.Articles, opt => opt.Ignore())
                .ReverseMap();
        }
    }
}
