﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DoC.MiniBlog.Common.Entities
{
    public class Comments
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        [StringLength(200)]
        public string Comment { get; set; }
        public string Username { get; set; }
        public Guid ArticleId { get; set; }
        [ForeignKey("ArticleId")]
        public Articles Articles { get; set; }
        public DateTime Time { get; set; }
    }
}
