﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DoC.MiniBlog.Common.Entities
{
    public class Articles
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        [StringLength(150)]
        public string Title { get; set; }
        [Column(TypeName = "text")]
        public string Body { get; set; }
        public DateTime PublishDate { get; set; }
        [StringLength(50)]
        public string Author { get; set; }
        public string Images { get; set; }
        public int CategoryId { get; set; }
        [ForeignKey("CategoryId")]
        public Categories Category { get; set; }
        public virtual List<ArticleLikes> ArticleLikes { get; set; }
        public virtual List<Comments> Comments { get; set; }
        public string Tags { get; set; }
    }
}